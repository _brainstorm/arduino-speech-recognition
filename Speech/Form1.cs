﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Speech.Recognition;

namespace Speech
{
    public partial class Form1 : Form
    {
        private SpeechRecognitionEngine rec;
        public Form1()
        {
            InitializeComponent();
            USB.Open();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            rec = new SpeechRecognitionEngine();
            rec.SetInputToDefaultAudioDevice();

            // Specify the exact words that the engine will try to recognize.
            Choices choices = new Choices("john", "offo");

            // Create and load a Grammar using the Choices above.
            GrammarBuilder grBuilder = new GrammarBuilder(choices);
            Grammar grammar = new Grammar(grBuilder);
            rec.LoadGrammar(grammar);
            
            // Create the event handler
            
            if (button1.Text == "Disable" )
            {
                button1.Text = "Enable";
                label5.Text = "Unsecured";
            }
            else
            {
                rec.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(rec_SpeechRecognized);
                rec.RecognizeAsync(RecognizeMode.Multiple);
                button1.Text = "Disable";
                label5.Text = "Secured";
            }
            

            
           
        }

        void rec_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            
            foreach (RecognizedWordUnit word in e.Result.Words)
            {
                switch (word.Text)
                {
                    case "john":
                        if (button1.Text == "Disable")
                        {
                            label6.Text = "john";
                            USB.Write("1");
                        }
                        break;
                    case "offo":
                        if (button1.Text == "Disable")
                        {
                            label6.Text = "offo";
                            USB.Write("0");
                        }
                        break;
                    default:
                        USB.Write("1");
                        break;
                }
                
            }
        }

          
        }
    
    }

