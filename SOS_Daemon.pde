const int ledPin =  13;      // the number of the LED pin
char val;

void sos () {
  for(int i=0; i<3; i++)
  {
    digitalWrite(ledPin,HIGH);
    delay(250);
    digitalWrite(ledPin,LOW);
    delay(250);
  }
  for(int i=0; i<3; i++)
  {
    digitalWrite(ledPin,HIGH);
    delay(500);
    digitalWrite(ledPin,LOW);
    delay(250);
  }
  for(int i=0; i<3; i++)
  {
    digitalWrite(ledPin,HIGH);
    delay(250);
    digitalWrite(ledPin,LOW);
    delay(250);
  }
}

void setup() 
{
// initialize the LED pin as an output:
pinMode(ledPin, OUTPUT); 
//initialize serial connection via USB
Serial.begin(9600);
}

void loop()
{
  int cont = 0;
  if (Serial.available())
  {
    val=Serial.read();
    if (val == '1')
    {
      sos();
      cont = 1;
    }
    else if(val == '0')
    {
      digitalWrite(ledPin,LOW);
      cont = 0;
    }
    else
    {
      if (cont == 1)
      {
        sos();
      }
      else
      {
        digitalWrite(ledPin,LOW);
      }
    }
      
    Serial.flush();
  }

}


